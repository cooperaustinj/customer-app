$(document).ready(function () {
    $('.delete-user').on('click', function () {
        if (confirm('Are you sure?')) {
            $.ajax({
                type: 'DELETE',
                url: '/users/delete/' + $(this).data('id'),
                success: function () {
                    window.location.replace('/');
                },
                error: function () {
                    alert('Error!');
                }
            });
        } else {
            return false;
        }
    });
});